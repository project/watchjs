WATCHJS
-------

This is a simple module that makes the jQuery Watch Plugin by Darcy Clarke
(http://darcyclarke.me/dev/watch/) available to module and theme developers.


INCLUDED FUNCTIONALITY
----------------------

* Makes jquery.watch.js available to other modules and custom code via the
  Library API

* Provides a drush command to download and install the latest version of the
  plugin


USE AND CONFIGURATION
---------------------

* After enabling this module, install the jquery.watch.js JavaScript file:

    * *Without drush:* Download jquery.watch.js and store it in your
      libraries directory. In most cases the resulting path should be
      sites/all/libraries/jquery.watch/jquery.watch.js

    * *With drush:* Run the command `drush watchjs-download`

* In your module code, add the library with libraries_load() and
  drupal_add_library(). If unsure where to do this, add it within an
  implementation of hook_init(). Use 'watchjs' for both the module and
  library name arguments, like this:

      libraries_load('watchjs');
      drupal_add_library('watchjs', 'watchjs');

* You can update your version of jquery.watch.js to the latest version by
  calling the drush command (drush watchjs-download).
